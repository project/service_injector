<?php

namespace Drupal\service_injector\Constant;

/**
 * Contains all string constants relating to Drupal Core services.
 *
 * Each constant is a direct match to its designated service ID. A class
 * reference is provided in the constant's docblock comment to allow quick
 * access to its service's class.
 *
 * Note that line comments are in use only for the time being. Once the list is
 * complete, these comments will be removed. They are used only to ensure that
 * the ordering of constants in this file matches the one from the Drupal
 * core.services.yml file.
 *
 * @see core/core.services.yml
 */
final class CoreServices {

  /**
   * The Drupal Cache Context IP service ID.
   *
   * @see \Drupal\Core\Cache\Context\IpCacheContext
   * @see \Drupal\service_injector\Service\CacheContextIpServiceTrait
   */
  public const CACHE_CONTEXT_IP = 'cache_context.ip';

  /**
   * The Drupal Cache Context Protocol Version service ID.
   *
   * @see \Drupal\Core\Cache\Context\ProtocolVersionCacheContext
   * @see \Drupal\service_injector\Service\CacheContextProtocolVersionServiceTrait
   */
  public const CACHE_CONTEXT_PROTOCOL_VERSION = 'cache_context.protocol_version';

  /**
   * The Drupal Cache Context Headers service ID.
   *
   * @see \Drupal\Core\Cache\Context\HeadersCacheContext
   * @see \Drupal\service_injector\Service\CacheContextHeadersServiceTrait
   */
  public const CACHE_CONTEXT_HEADERS = 'cache_context.headers';

  /**
   * The Drupal Cache Context Cookies service ID.
   *
   * @see \Drupal\Core\Cache\Context\CookiesCacheContext
   * @see \Drupal\service_injector\Service\CacheContextCookiesServiceTrait
   */
  public const CACHE_CONTEXT_COOKIES = 'cache_context.cookies';

  /**
   * The Drupal Cache Context Session service ID.
   *
   * @see \Drupal\Core\Cache\Context\SessionCacheContext
   * @see \Drupal\service_injector\Service\CacheContextSessionServiceTrait
   */
  public const CACHE_CONTEXT_SESSION = 'cache_context.session';

  /**
   * The Drupal Cache Context Session Exists service ID.
   *
   * @see \Drupal\Core\Cache\Context\SessionExistsCacheContext
   * @see \Drupal\service_injector\Service\CacheContextSessionExistsServiceTrait
   */
  public const CACHE_CONTEXT_SESSION_EXISTS = 'cache_context.session.exists';

  /**
   * The Drupal Cache Context Request Format service ID.
   *
   * @see \Drupal\Core\Cache\Context\RequestFormatCacheContext
   * @see \Drupal\service_injector\Service\CacheContextRequestFormatServiceTrait
   */
  public const CACHE_CONTEXT_REQUEST_FORMAT = 'cache_context.request_format';

  /**
   * The Drupal Cache Context URL service ID.
   *
   * @see \Drupal\Core\Cache\Context\UrlCacheContext
   * @see \Drupal\service_injector\Service\CacheContextUrlServiceTrait
   */
  public const CACHE_CONTEXT_URL = 'cache_context.url';

  /**
   * The Drupal Cache Context URL Site service ID.
   *
   * @see \Drupal\Core\Cache\Context\SiteCacheContext
   * @see \Drupal\service_injector\Service\CacheContextUrlSiteServiceTrait
   */
  public const CACHE_CONTEXT_URL_SITE = 'cache_context.url.site';

  /**
   * The Drupal Cache Context URL Path service ID.
   *
   * @see \Drupal\Core\Cache\Context\PathCacheContext
   * @see \Drupal\service_injector\Service\CacheContextUrlPathServiceTrait
   */
  public const CACHE_CONTEXT_URL_PATH = 'cache_context.url.path';

  /**
   * The Drupal Cache Context URL Path Parent service ID.
   *
   * @see \Drupal\Core\Cache\Context\PathParentCacheContext
   * @see \Drupal\service_injector\Service\CacheContextUrlPathParentServiceTrait
   */
  public const CACHE_CONTEXT_URL_PATH_PARENT = 'cache_context.url.path.parent';

  /**
   * The Drupal Cache Context URL Path Is Front service ID.
   *
   * @see \Drupal\Core\Cache\Context\IsFrontPathCacheContext
   * @see \Drupal\service_injector\Service\CacheContextUrlPathIsFrontServiceTrait
   */
  public const CACHE_CONTEXT_URL_PATH_IS_FRONT = 'cache_context.url.path.is_front';

  /**
   * The Drupal Cache Context URL Query Args service ID.
   *
   * @see \Drupal\Core\Cache\Context\QueryArgsCacheContext
   * @see \Drupal\service_injector\Service\CacheContextUrlQueryArgsServiceTrait
   */
  public const CACHE_CONTEXT_URL_QUERY_ARGS = 'cache_context.url.query_args';

  /**
   * The Drupal Cache Context URL Query Args Pagers service ID.
   *
   * @see \Drupal\Core\Cache\Context\PagersCacheContext
   * @see \Drupal\service_injector\Service\CacheContextUrlQueryArgsPagersServiceTrait
   */
  public const CACHE_CONTEXT_URL_QUERY_ARGS_PAGERS = 'cache_context.url.query_args.pagers';

  /**
   * The Drupal Cache Context Route service ID.
   *
   * @see \Drupal\Core\Cache\Context\RouteCacheContext
   * @see \Drupal\service_injector\Service\CacheContextRouteServiceTrait
   */
  public const CACHE_CONTEXT_ROUTE = 'cache_context.route';

  /**
   * The Drupal Cache Context Route Name service ID.
   *
   * @see \Drupal\Core\Cache\Context\RouteNameCacheContext
   * @see \Drupal\service_injector\Service\CacheContextRouteNameServiceTrait
   */
  public const CACHE_CONTEXT_ROUTE_NAME = 'cache_context.route.name';

  /**
   * The Drupal Cache Context Route Menu Active Trails service ID.
   *
   * @see \Drupal\Core\Cache\Context\MenuActiveTrailsCacheContext
   * @see \Drupal\service_injector\Service\CacheContextRouteMenuActiveTrailsServiceTrait
   */
  public const CACHE_CONTEXT_ROUTE_MENU_ACTIVE_TRAILS = 'cache_context.route.menu_active_trails';

  /**
   * The Drupal Cache Context User service ID.
   *
   * @see \Drupal\Core\Cache\Context\UserCacheContext
   * @see \Drupal\service_injector\Service\CacheContextUserServiceTrait
   */
  public const CACHE_CONTEXT_USER = 'cache_context.user';

  /**
   * The Drupal Cache Context User Permissions service ID.
   *
   * @see \Drupal\Core\Cache\Context\AccountPermissionsCacheContext
   * @see \Drupal\service_injector\Service\CacheContextUserPermissionsServiceTrait
   */
  public const CACHE_CONTEXT_USER_PERMISSIONS = 'cache_context.user.permissions';

  /**
   * The Drupal Cache Context User Roles service ID.
   *
   * @see \Drupal\Core\Cache\Context\UserRolesCacheContext
   * @see \Drupal\service_injector\Service\CacheContextUserRolesServiceTrait
   */
  public const CACHE_CONTEXT_USER_ROLES = 'cache_context.user.roles';

  /**
   * The Drupal Cache Context User Is Super User service ID.
   *
   * @see \Drupal\Core\Cache\Context\IsSuperUserCacheContext
   * @see \Drupal\service_injector\Service\CacheContextUserIsSuperUserServiceTrait
   */
  public const CACHE_CONTEXT_USER_IS_SUPER_USER = 'cache_context.user.is_super_user';

  /**
   * The Drupal Cache Context Languages service ID.
   *
   * @see \Drupal\Core\Cache\Context\LanguagesCacheContext
   * @see \Drupal\service_injector\Service\CacheContextLanguagesServiceTrait
   */
  public const CACHE_CONTEXT_LANGUAGES = 'cache_context.languages';

  /**
   * The Drupal Cache Context Theme service ID.
   *
   * @see \Drupal\Core\Cache\Context\ThemeCacheContext
   * @see \Drupal\service_injector\Service\CacheContextThemeServiceTrait
   */
  public const CACHE_CONTEXT_THEME = 'cache_context.theme';

  /**
   * The Drupal Cache Context Time Zone service ID.
   *
   * @see \Drupal\Core\Cache\Context\TimeZoneCacheContext
   * @see \Drupal\service_injector\Service\CacheContextTimeZoneServiceTrait
   */
  public const CACHE_CONTEXT_TIMEZONE = 'cache_context.timezone';

  /**
   * The Drupal Cache Factory service ID.
   *
   * @see \Drupal\Core\Cache\CacheFactory
   * @see \Drupal\service_injector\Service\CacheFactoryServiceTrait
   */
  public const CACHE_FACTORY = 'cache_factory';

  /**
   * The Drupal Cache Contexts Manager service ID.
   *
   * @see \Drupal\Core\Cache\Context\CacheContextsManager
   * @see \Drupal\service_injector\Service\CacheContextsManagerServiceTrait
   */
  public const CACHE_CONTEXTS_MANAGER = 'cache_contexts_manager';

  /**
   * The Drupal Cache Tags Invalidator service ID.
   *
   * @see \Drupal\Core\Cache\CacheTagsInvalidator
   * @see \Drupal\service_injector\Service\CacheTagsInvalidatorServiceTrait
   */
  public const CACHE_TAGS_INVALIDATOR = 'cache_tags.invalidator';

  /**
   * The Drupal Cache Tags Invalidator Checksum service ID.
   *
   * @see \Drupal\Core\Cache\DatabaseCacheTagsChecksum
   * @see \Drupal\service_injector\Service\CacheTagsInvalidatorChecksumServiceTrait
   */
  public const CACHE_TAGS_INVALIDATOR_CHECKSUM = 'cache_tags.invalidator.checksum';

  /**
   * The Drupal Cache Backend Chained Fast service ID.
   *
   * @see \Drupal\Core\Cache\ChainedFastBackendFactory
   * @see \Drupal\service_injector\Service\CacheBackendChainedFastServiceTrait
   */
  public const CACHE_BACKEND_CHAINEDFAST = 'cache.backend.chainedfast';

  /**
   * The Drupal Cache Backend Database service ID.
   *
   * @see \Drupal\Core\Cache\DatabaseBackendFactory
   * @see \Drupal\service_injector\Service\CacheBackendDatabaseServiceTrait
   */
  public const CACHE_BACKEND_DATABASE = 'cache.backend.database';

  /**
   * The Drupal Cache Backend APCu service ID.
   *
   * @see \Drupal\Core\Cache\ApcuBackendFactory
   * @see \Drupal\service_injector\Service\CacheBackendApcuServiceTrait
   */
  public const CACHE_BACKEND_APCU = 'cache.backend.apcu';

  /**
   * The Drupal Cache Backend PHP service ID.
   *
   * @see \Drupal\Core\Cache\PhpBackendFactory
   * @see \Drupal\service_injector\Service\CacheBackendPhpServiceTrait
   */
  public const CACHE_BACKEND_PHP = 'cache.backend.php';

  /**
   * The Drupal Cache Backend Memory service ID.
   *
   * @see \Drupal\Core\Cache\MemoryBackendFactory
   * @see \Drupal\service_injector\Service\CacheBackendMemoryServiceTrait
   */
  public const CACHE_BACKEND_MEMORY = 'cache.backend.memory';

  /**
   * The Drupal Cache Static service ID.
   *
   * @see \Drupal\Core\Cache\CacheBackendInterface
   * @see \Drupal\service_injector\Service\CacheStaticServiceTrait
   */
  public const CACHE_STATIC = 'cache.static';

  /**
   * The Drupal Cache Bootstrap service ID.
   *
   * @see \Drupal\Core\Cache\CacheBackendInterface
   * @see \Drupal\service_injector\Service\CacheBootstrapServiceTrait
   */
  public const CACHE_BOOTSTRAP = 'cache.bootstrap';

  /**
   * The Drupal Cache Config service ID.
   *
   * @see \Drupal\Core\Cache\CacheBackendInterface
   * @see \Drupal\service_injector\Service\CacheConfigServiceTrait
   */
  public const CACHE_CONFIG = 'cache.config';

  /**
   * The Drupal Cache Default service ID.
   *
   * @see \Drupal\Core\Cache\CacheBackendInterface
   * @see \Drupal\service_injector\Service\CacheDefaultServiceTrait
   */
  public const CACHE_DEFAULT = 'cache.default';

  /**
   * The Drupal Cache Entity service ID.
   *
   * @see \Drupal\Core\Cache\CacheBackendInterface
   * @see \Drupal\service_injector\Service\CacheEntityServiceTrait
   */
  public const CACHE_ENTITY = 'cache.entity';

  /**
   * The Drupal Cache Menu service ID.
   *
   * @see \Drupal\Core\Cache\CacheBackendInterface
   * @see \Drupal\service_injector\Service\CacheMenuServiceTrait
   */
  public const CACHE_MENU = 'cache.menu';

  /**
   * The Drupal Cache Render service ID.
   *
   * @see \Drupal\Core\Cache\CacheBackendInterface
   * @see \Drupal\service_injector\Service\CacheRenderServiceTrait
   */
  public const CACHE_RENDER = 'cache.render';

  /**
   * The Drupal Cache Data service ID.
   *
   * @see \Drupal\Core\Cache\CacheBackendInterface
   * @see \Drupal\service_injector\Service\CacheDataServiceTrait
   */
  public const CACHE_DATA = 'cache.data';

  /**
   * The Drupal Cache Discovery service ID.
   *
   * @see \Drupal\Core\Cache\CacheBackendInterface
   * @see \Drupal\service_injector\Service\CacheDiscoveryServiceTrait
   */
  public const CACHE_DISCOVERY = 'cache.discovery';

  /**
   * The Drupal Cache Router Rebuild Subscriber service ID.
   *
   * @see \Drupal\Core\EventSubscriber\CacheRouterRebuildSubscriber
   * @see \Drupal\service_injector\Service\CacheRouterRebuildSubscriberServiceTrait
   */
  public const CACHE_ROUTER_REBUILD_SUBSCRIBER = 'cache_router_rebuild_subscriber';

  /**
   * The Drupal Page Cache Request Policy service ID.
   *
   * @see \Drupal\Core\PageCache\DefaultRequestPolicy
   * @see \Drupal\service_injector\Service\PageCacheRequestPolicyServiceTrait
   */
  public const PAGE_CACHE_REQUEST_POLICY = 'page_cache_request_policy';

  /**
   * The Drupal Page Cache Response Policy service ID.
   *
   * @see \Drupal\Core\PageCache\ChainResponsePolicy
   * @see \Drupal\service_injector\Service\PageCacheResponsePolicyServiceTrait
   */
  public const PAGE_CACHE_RESPONSE_POLICY = 'page_cache_response_policy';

  /**
   * The Drupal Page Cache Kill Switch service ID.
   *
   * @see \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   * @see \Drupal\service_injector\Service\PageCacheKillSwitchServiceTrait
   */
  public const PAGE_CACHE_KILL_SWITCH = 'page_cache_kill_switch';

  /**
   * The Drupal Page Cache No Cache Routes service ID.
   *
   * @see \Drupal\Core\PageCache\ResponsePolicy\DenyNoCacheRoutes
   * @see \Drupal\service_injector\Service\PageCacheNoCacheRoutesServiceTrait
   */
  public const PAGE_CACHE_NO_CACHE_ROUTES = 'page_cache_no_cache_routes';

  /**
   * The Drupal Page Cache No Server Error service ID.
   *
   * @see \Drupal\Core\PageCache\ResponsePolicy\NoServerError
   * @see \Drupal\service_injector\Service\PageCacheNoServerErrorServiceTrait
   */
  public const PAGE_CACHE_NO_SERVER_ERROR = 'page_cache_no_server_error';

  /**
   * The Drupal Config Manager service ID.
   *
   * @see \Drupal\Core\Config\ConfigManager
   * @see \Drupal\service_injector\Service\ConfigManagerServiceTrait
   */
  public const CONFIG_MANAGER = 'config.manager';

  /**
   * The Drupal Config Factory service ID.
   *
   * @see \Drupal\Core\Config\ConfigFactoryInterface
   * @see \Drupal\service_injector\Service\ConfigFactoryServiceTrait
   */
  public const CONFIG_FACTORY = 'config.factory';

  /**
   * The Drupal Config Importer Subscriber service ID.
   *
   * @see \Drupal\Core\Config\Importer\FinalMissingContentSubscriber
   * @see \Drupal\service_injector\Service\ConfigImporterSubscriberServiceTrait
   */
  public const CONFIG_IMPORTER_SUBSCRIBER = 'config.importer_subscriber';

  /**
   * The Drupal Config Installer service ID.
   *
   * @see \Drupal\Core\Config\ConfigInstaller
   * @see \Drupal\service_injector\Service\ConfigInstallerServiceTrait
   */
  public const CONFIG_INSTALLER = 'config.installer';

  /**
   * The Drupal Serialization JSON service ID.
   *
   * Line 430.
   *
   * @see \Drupal\Component\Serialization\Json
   * @see \Drupal\service_injector\Service\SerializationJsonServiceTrait
   */
  public const SERIALIZATION_JSON = 'serialization.json';

  /**
   * The Drupal State service ID.
   *
   * Line 440.
   *
   * @see \Drupal\Core\State\State
   * @see \Drupal\service_injector\Service\StateServiceTrait
   */
  public const STATE = 'state';

  /**
   * The Drupal Path Current service ID.
   *
   * Line 459.
   *
   * @see \Drupal\Core\Path\CurrentPathStack
   * @see \Drupal\service_injector\Service\PathCurrentServiceTrait
   */
  public const PATH_CURRENT = 'path.current';

  /**
   * The Drupal Module Installer service ID.
   *
   * Line 508
   *
   * @see \Drupal\Core\Extension\ModuleInstaller
   * @see \Drupal\service_injector\Service\ModuleInstallerServiceTrait
   */
  public const MODULE_INSTALLER = 'module_installer';

  /**
   * The Drupal Entity Type Manager service ID.
   *
   * Line 553.
   *
   * @see \Drupal\Core\Entity\EntityTypeManager
   * @see \Drupal\service_injector\Service\EntityTypeManagerServiceTrait
   */
  public const ENTITY_TYPE_MANAGER = 'entity_type.manager';

  /**
   * The Symfony Request Stack service ID.
   *
   * Line 686.
   *
   * @see \Symfony\Component\HttpFoundation\RequestStack
   * @see \Drupal\service_injector\Service\RequestStackServiceTrait
   */
  public const REQUEST_STACK = 'request_stack';

  /**
   * The Drupal Language Manager service ID.
   *
   * Line 783.
   *
   * @see \Drupal\Core\Language\LanguageManager
   * @see \Drupal\service_injector\Service\LanguageManagerServiceTrait
   */
  public const LANGUAGE_MANAGER = 'language_manager';

  /**
   * The Drupal Path Matcher service ID.
   *
   * Line 956.
   *
   * @see \Drupal\Core\Path\PathMatcher
   * @see \Drupal\service_injector\Service\PathMatcherServiceTrait
   */
  public const PATH_MATCHER = 'path.matcher';

  /**
   * The Drupal Plugin Manager Mail service ID.
   *
   * Line 1356.
   *
   * @see \Drupal\Core\Mail\MailManager
   * @see \Drupal\service_injector\Service\PluginManagerMailServiceTrait
   */
  public const PLUGIN_MANAGER_MAIL = 'plugin.manager.mail';

  /**
   * The Drupal Email Validator service ID.
   *
   * Line 1692.
   *
   * @see \Drupal\Component\Utility\EmailValidator
   * @see \Drupal\service_injector\Service\EmailValidatorServiceTrait
   */
  public const EMAIL_VALIDATOR = 'email.validator';

}
