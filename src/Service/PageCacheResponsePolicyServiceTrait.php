<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\PageCache\ChainResponsePolicy;

/**
 * Injection utility for the Drupal Page Cache Response Policy service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::PAGE_CACHE_RESPONSE_POLICY
 */
trait PageCacheResponsePolicyServiceTrait {

  /**
   * The Drupal Page Cache Response Policy service.
   *
   * @var \Drupal\Core\PageCache\ChainResponsePolicy
   */
  private ChainResponsePolicy $pageCacheResponsePolicyService;

  /**
   * Gets the Drupal Page Cache Response Policy service.
   *
   * @return \Drupal\Core\PageCache\ChainResponsePolicy
   *   The Drupal Page Cache Response Policy service.
   */
  public function pageCacheResponsePolicyService() : ChainResponsePolicy {
    return $this->pageCacheResponsePolicyService;
  }

  /**
   * Sets the Drupal Page Cache Response Policy service.
   *
   * @param \Drupal\Core\PageCache\ChainResponsePolicy $service
   *   The service to be set.
   */
  public function setPageCacheResponsePolicyService(ChainResponsePolicy $service) : void {
    $this->pageCacheResponsePolicyService = $service;
  }

}
