<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\TimeZoneCacheContext;

/**
 * Injection utility for the Drupal Cache Context Time Zone service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_TIMEZONE
 */
trait CacheContextTimeZoneServiceTrait {

  /**
   * The Drupal Cache Context Time Zone service.
   *
   * @var \Drupal\Core\Cache\Context\TimeZoneCacheContext
   */
  private TimeZoneCacheContext $cacheContextTimeZoneService;

  /**
   * Gets the Drupal Cache Context Time Zone service.
   *
   * @return \Drupal\Core\Cache\Context\TimeZoneCacheContext
   *   The Drupal Cache Context Time Zone service.
   */
  public function cacheContextTimeZoneService() : TimeZoneCacheContext {
    return $this->cacheContextTimeZoneService;
  }

  /**
   * Sets the Drupal Cache Context Time Zone service.
   *
   * @param \Drupal\Core\Cache\Context\TimeZoneCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextTimeZoneService(TimeZoneCacheContext $service) : void {
    $this->cacheContextTimeZoneService = $service;
  }

}
