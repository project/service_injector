<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\MemoryBackendFactory;

/**
 * Injection utility for the Drupal Cache Backend Memory service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_BACKEND_MEMORY
 */
trait CacheBackendMemoryServiceTrait {

  /**
   * The Drupal Cache Backend Memory service.
   *
   * @var \Drupal\Core\Cache\MemoryBackendFactory
   */
  private MemoryBackendFactory $cacheBackendMemoryService;

  /**
   * Gets the Drupal Cache Backend Memory service.
   *
   * @return \Drupal\Core\Cache\MemoryBackendFactory
   *   The Drupal Cache Backend Memory service.
   */
  public function cacheBackendMemoryService() : MemoryBackendFactory {
    return $this->cacheBackendMemoryService;
  }

  /**
   * Sets the Drupal Cache Backend Memory service.
   *
   * @param \Drupal\Core\Cache\MemoryBackendFactory $service
   *   The service to be set.
   */
  public function setCacheBackendMemoryService(MemoryBackendFactory $service) : void {
    $this->cacheBackendMemoryService = $service;
  }

}
