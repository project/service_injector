<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\PathParentCacheContext;

/**
 * Injection utility for the Drupal Cache Context URL Path Parent service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_URL_PATH_PARENT
 */
trait CacheContextUrlPathParentServiceTrait {

  /**
   * The Drupal Cache Context URL Path Parent service.
   *
   * @var \Drupal\Core\Cache\Context\PathParentCacheContext
   */
  private PathParentCacheContext $cacheContextUrlPathParentService;

  /**
   * Gets the Drupal Cache Context URL Path Parent service.
   *
   * @return \Drupal\Core\Cache\Context\PathParentCacheContext
   *   The Drupal Cache Context URL Path Parent service.
   */
  public function cacheContextUrlPathParentService() : PathParentCacheContext {
    return $this->cacheContextUrlPathParentService;
  }

  /**
   * Sets the Drupal Cache Context URL Path Parent service.
   *
   * @param \Drupal\Core\Cache\Context\PathParentCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextUrlPathParentService(PathParentCacheContext $service) : void {
    $this->cacheContextUrlPathParentService = $service;
  }

}
