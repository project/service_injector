<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Entity\EntityTypeManager;

/**
 * Injection utility for the Drupal Entity Type Manager service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::ENTITY_TYPE_MANAGER
 */
trait EntityTypeManagerServiceTrait {

  /**
   * The Drupal Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  private EntityTypeManager $entityTypeManagerService;

  /**
   * Gets the Drupal Entity Type Manager service.
   *
   * @return \Drupal\Core\Entity\EntityTypeManager
   *   The Drupal Entity Type Manager service.
   */
  public function entityTypeManagerService() : EntityTypeManager {
    return $this->entityTypeManagerService;
  }

  /**
   * Sets the Drupal Entity Type Manager service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $service
   *   The service to be set.
   */
  public function setEntityTypeManagerService(EntityTypeManager $service) : void {
    $this->entityTypeManagerService = $service;
  }

}
