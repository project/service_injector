<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;

/**
 * Injection utility for the Drupal Page Cache Kill Switch service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::PAGE_CACHE_KILL_SWITCH
 */
trait PageCacheKillSwitchServiceTrait {

  /**
   * The Drupal Page Cache Kill Switch service.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  private KillSwitch $pageCacheKillSwitchService;

  /**
   * Gets the Drupal Page Cache Kill Switch service.
   *
   * @return \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   *   The Drupal Page Cache Kill Switch service.
   */
  public function pageCacheKillSwitchService() : KillSwitch {
    return $this->pageCacheKillSwitchService;
  }

  /**
   * Sets the Drupal Page Cache Kill Switch service.
   *
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $service
   *   The service to be set.
   */
  public function setPageCacheKillSwitchService(KillSwitch $service) : void {
    $this->pageCacheKillSwitchService = $service;
  }

}
