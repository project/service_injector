<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\RouteCacheContext;

/**
 * Injection utility for Drupal Cache Context Route Menu Active Trails.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_ROUTE_MENU_ACTIVE_TRAILS
 */
trait CacheContextRouteMenuActiveTrailsServiceTrait {

  /**
   * The Drupal Cache Context Route Menu Active Trails service.
   *
   * @var \Drupal\Core\Cache\Context\RouteCacheContext
   */
  private RouteCacheContext $cacheContextRouteMenuActiveTrailsService;

  /**
   * Gets the Drupal Cache Context Route Menu Active Trails service.
   *
   * @return \Drupal\Core\Cache\Context\RouteCacheContext
   *   The Drupal Cache Context Route Menu Active Trails service.
   */
  public function cacheContextRouteMenuActiveTrailsService() : RouteCacheContext {
    return $this->cacheContextRouteMenuActiveTrailsService;
  }

  /**
   * Sets the Drupal Cache Context Route Menu Active Trails service.
   *
   * @param \Drupal\Core\Cache\Context\RouteCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextRouteMenuActiveTrailsService(RouteCacheContext $service) : void {
    $this->cacheContextRouteMenuActiveTrailsService = $service;
  }

}
