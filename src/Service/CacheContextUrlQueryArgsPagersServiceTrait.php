<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\PagersCacheContext;

/**
 * Injection utility for the Drupal Cache Context URL Query Args Pagers service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_URL_QUERY_ARGS_PAGERS
 */
trait CacheContextUrlQueryArgsPagersServiceTrait {

  /**
   * The Drupal Cache Context URL Query Args Pagers service.
   *
   * @var \Drupal\Core\Cache\Context\PagersCacheContext
   */
  private PagersCacheContext $cacheContextUrlQueryArgsPagersService;

  /**
   * Gets the Drupal Cache Context URL Query Args Pagers service.
   *
   * @return \Drupal\Core\Cache\Context\PagersCacheContext
   *   The Drupal Cache Context URL Query Args Pagers service.
   */
  public function cacheContextUrlQueryArgsPagersService() : PagersCacheContext {
    return $this->cacheContextUrlQueryArgsPagersService;
  }

  /**
   * Sets the Drupal Cache Context URL Query Args Pagers service.
   *
   * @param \Drupal\Core\Cache\Context\PagersCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextUrlQueryArgsPagersService(PagersCacheContext $service) : void {
    $this->cacheContextUrlQueryArgsPagersService = $service;
  }

}
