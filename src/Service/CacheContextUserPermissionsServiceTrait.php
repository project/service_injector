<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\AccountPermissionsCacheContext;

/**
 * Injection utility for the Drupal Cache Context User Permissions service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_USER_PERMISSIONS
 */
trait CacheContextUserPermissionsServiceTrait {

  /**
   * The Drupal Cache Context User Permissions service.
   *
   * @var \Drupal\Core\Cache\Context\AccountPermissionsCacheContext
   */
  private AccountPermissionsCacheContext $cacheContextUserPermissionsService;

  /**
   * Gets the Drupal Cache Context User Permissions service.
   *
   * @return \Drupal\Core\Cache\Context\AccountPermissionsCacheContext
   *   The Drupal Cache Context User Permissions service.
   */
  public function cacheContextUserPermissionsService() : AccountPermissionsCacheContext {
    return $this->cacheContextUserPermissionsService;
  }

  /**
   * Sets the Drupal Cache Context User Permissions service.
   *
   * @param \Drupal\Core\Cache\Context\AccountPermissionsCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextUserPermissionsService(AccountPermissionsCacheContext $service) : void {
    $this->cacheContextUserPermissionsService = $service;
  }

}
