<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\CacheFactory;

/**
 * Injection utility for the Drupal Cache Factory service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_FACTORY
 */
trait CacheFactoryServiceTrait {

  /**
   * The Drupal Cache Factory service.
   *
   * @var \Drupal\Core\Cache\CacheFactory
   */
  private CacheFactory $cacheFactoryService;

  /**
   * Gets the Drupal Cache Factory service.
   *
   * @return \Drupal\Core\Cache\CacheFactory
   *   The Drupal Cache Factory service.
   */
  public function cacheFactoryService() : CacheFactory {
    return $this->cacheFactoryService;
  }

  /**
   * Sets the Drupal Cache Factory service.
   *
   * @param \Drupal\Core\Cache\CacheFactory $service
   *   The service to be set.
   */
  public function setCacheFactoryService(CacheFactory $service) : void {
    $this->cacheFactoryService = $service;
  }

}
