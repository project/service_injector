<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\QueryArgsCacheContext;

/**
 * Injection utility for the Drupal Cache Context URL Query Args service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_URL_QUERY_ARGS
 */
trait CacheContextUrlQueryArgsServiceTrait {

  /**
   * The Drupal Cache Context URL Query Args service.
   *
   * @var \Drupal\Core\Cache\Context\QueryArgsCacheContext
   */
  private QueryArgsCacheContext $cacheContextUrlQueryArgsService;

  /**
   * Gets the Drupal Cache Context URL Query Args service.
   *
   * @return \Drupal\Core\Cache\Context\QueryArgsCacheContext
   *   The Drupal Cache Context URL Query Args service.
   */
  public function cacheContextUrlQueryArgsService() : QueryArgsCacheContext {
    return $this->cacheContextUrlQueryArgsService;
  }

  /**
   * Sets the Drupal Cache Context URL Query Args service.
   *
   * @param \Drupal\Core\Cache\Context\QueryArgsCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextUrlQueryArgsService(QueryArgsCacheContext $service) : void {
    $this->cacheContextUrlQueryArgsService = $service;
  }

}
