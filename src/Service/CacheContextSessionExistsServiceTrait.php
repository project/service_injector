<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\SessionExistsCacheContext;

/**
 * Injection utility for the Drupal Cache Context Session Exists service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_SESSION_EXISTS
 */
trait CacheContextSessionExistsServiceTrait {

  /**
   * The Drupal Cache Context Session Exists service.
   *
   * @var \Drupal\Core\Cache\Context\SessionExistsCacheContext
   */
  private SessionExistsCacheContext $cacheContextSessionExistsService;

  /**
   * Gets the Drupal Cache Context Session Exists service.
   *
   * @return \Drupal\Core\Cache\Context\SessionExistsCacheContext
   *   The Drupal Cache Context Session Exists service.
   */
  public function cacheContextSessionExistsService() : SessionExistsCacheContext {
    return $this->cacheContextSessionExistsService;
  }

  /**
   * Sets the Drupal Cache Context Session Exists service.
   *
   * @param \Drupal\Core\Cache\Context\SessionExistsCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextSessionExistsService(SessionExistsCacheContext $service) : void {
    $this->cacheContextSessionExistsService = $service;
  }

}
