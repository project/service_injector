<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\PathCacheContext;

/**
 * Injection utility for the Drupal Cache Context URL Path service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_URL_PATH
 */
trait CacheContextUrlPathServiceTrait {

  /**
   * The Drupal Cache Context URL Path service.
   *
   * @var \Drupal\Core\Cache\Context\PathCacheContext
   */
  private PathCacheContext $cacheContextUrlPathService;

  /**
   * Gets the Drupal Cache Context URL Path service.
   *
   * @return \Drupal\Core\Cache\Context\PathCacheContext
   *   The Drupal Cache Context URL Path service.
   */
  public function cacheContextUrlPathService() : PathCacheContext {
    return $this->cacheContextUrlPathService;
  }

  /**
   * Sets the Drupal Cache Context URL Path service.
   *
   * @param \Drupal\Core\Cache\Context\PathCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextUrlPathService(PathCacheContext $service) : void {
    $this->cacheContextUrlPathService = $service;
  }

}
