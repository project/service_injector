<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Injection utility for the Drupal Cache Default service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_DEFAULT
 */
trait CacheDefaultServiceTrait {

  /**
   * The Drupal Cache Default service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private CacheBackendInterface $cacheDefaultService;

  /**
   * Gets the Drupal Cache Default service.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The Drupal Cache Default service.
   */
  public function cacheDefaultService() : CacheBackendInterface {
    return $this->cacheDefaultService;
  }

  /**
   * Sets the Drupal Cache Default service.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $service
   *   The service to be set.
   */
  public function setCacheDefaultService(CacheBackendInterface $service) : void {
    $this->cacheDefaultService = $service;
  }

}
