<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\PhpBackendFactory;

/**
 * Injection utility for the Drupal Cache Backend PHP service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_BACKEND_PHP
 */
trait CacheBackendPhpServiceTrait {

  /**
   * The Drupal Cache Backend PHP service.
   *
   * @var \Drupal\Core\Cache\PhpBackendFactory
   */
  private PhpBackendFactory $cacheBackendPhpService;

  /**
   * Gets the Drupal Cache Backend PHP service.
   *
   * @return \Drupal\Core\Cache\PhpBackendFactory
   *   The Drupal Cache Backend PHP service.
   */
  public function cacheBackendPhpService() : PhpBackendFactory {
    return $this->cacheBackendPhpService;
  }

  /**
   * Sets the Drupal Cache Backend PHP service.
   *
   * @param \Drupal\Core\Cache\PhpBackendFactory $service
   *   The service to be set.
   */
  public function setCacheBackendPhpService(PhpBackendFactory $service) : void {
    $this->cacheBackendPhpService = $service;
  }

}
