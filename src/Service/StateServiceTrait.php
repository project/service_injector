<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\State\State;

/**
 * Injection utility for the Drupal State service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::STATE
 */
trait StateServiceTrait {

  /**
   * The Drupal State service.
   *
   * @var \Drupal\Core\State\State
   */
  private State $stateService;

  /**
   * Gets the Drupal State service.
   *
   * @return \Drupal\Core\State\State
   *   The Drupal State service.
   */
  public function stateService() : State {
    return $this->stateService;
  }

  /**
   * Sets the Drupal State service.
   *
   * @param \Drupal\Core\State\State $service
   *   The service to be set.
   */
  public function setStateService(State $service) : void {
    $this->stateService = $service;
  }

}
