<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Injection utility for the Drupal Cache Render service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_RENDER
 */
trait CacheRenderServiceTrait {

  /**
   * The Drupal Cache Render service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private CacheBackendInterface $cacheRenderService;

  /**
   * Gets the Drupal Cache Render service.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The Drupal Cache Render service.
   */
  public function cacheRenderService() : CacheBackendInterface {
    return $this->cacheRenderService;
  }

  /**
   * Sets the Drupal Cache Render service.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $service
   *   The service to be set.
   */
  public function setCacheRenderService(CacheBackendInterface $service) : void {
    $this->cacheRenderService = $service;
  }

}
