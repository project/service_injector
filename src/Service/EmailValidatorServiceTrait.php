<?php

namespace Drupal\service_injector\Service;

use Drupal\Component\Utility\EmailValidator;

/**
 * Injection utility for the Drupal Email Validator service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::EMAIL_VALIDATOR
 */
trait EmailValidatorServiceTrait {

  /**
   * The Drupal Email Validator service.
   *
   * @var \Drupal\Component\Utility\EmailValidator
   */
  private EmailValidator $emailValidatorService;

  /**
   * Gets the Drupal Email Validator service.
   *
   * @return \Drupal\Component\Utility\EmailValidator
   *   The Drupal Email Validator service.
   */
  public function emailValidatorService() : EmailValidator {
    return $this->emailValidatorService;
  }

  /**
   * Sets the Drupal Email Validator service.
   *
   * @param \Drupal\Component\Utility\EmailValidator $service
   *   The service to be set.
   */
  public function setEmailValidatorService(EmailValidator $service) : void {
    $this->emailValidatorService = $service;
  }

}
