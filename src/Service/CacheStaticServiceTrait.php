<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Injection utility for the Drupal Cache Static service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_STATIC
 */
trait CacheStaticServiceTrait {

  /**
   * The Drupal Cache Static service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private CacheBackendInterface $cacheStaticService;

  /**
   * Gets the Drupal Cache Static service.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The Drupal Cache Static service.
   */
  public function cacheStaticService() : CacheBackendInterface {
    return $this->cacheStaticService;
  }

  /**
   * Sets the Drupal Cache Static service.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $service
   *   The service to be set.
   */
  public function setCacheStaticService(CacheBackendInterface $service) : void {
    $this->cacheStaticService = $service;
  }

}
