<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\RequestFormatCacheContext;

/**
 * Injection utility for the Drupal Cache Context Request Format service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_REQUEST_FORMAT
 */
trait CacheContextRequestFormatServiceTrait {

  /**
   * The Drupal Cache Context Request Format service.
   *
   * @var \Drupal\Core\Cache\Context\RequestFormatCacheContext
   */
  private RequestFormatCacheContext $cacheContextRequestFormatService;

  /**
   * Gets the Drupal Cache Context Request Format service.
   *
   * @return \Drupal\Core\Cache\Context\RequestFormatCacheContext
   *   The Drupal Cache Context Request Format service.
   */
  public function cacheContextRequestFormatService() : RequestFormatCacheContext {
    return $this->cacheContextRequestFormatService;
  }

  /**
   * Sets the Drupal Cache Context Request Format service.
   *
   * @param \Drupal\Core\Cache\Context\RequestFormatCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextRequestFormatService(RequestFormatCacheContext $service) : void {
    $this->cacheContextRequestFormatService = $service;
  }

}
