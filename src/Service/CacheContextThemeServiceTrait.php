<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\ThemeCacheContext;

/**
 * Injection utility for the Drupal Cache Context Theme service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_THEME
 */
trait CacheContextThemeServiceTrait {

  /**
   * The Drupal Cache Context Theme service.
   *
   * @var \Drupal\Core\Cache\Context\ThemeCacheContext
   */
  private ThemeCacheContext $cacheContextThemeService;

  /**
   * Gets the Drupal Cache Context Theme service.
   *
   * @return \Drupal\Core\Cache\Context\ThemeCacheContext
   *   The Drupal Cache Context Theme service.
   */
  public function cacheContextThemeService() : ThemeCacheContext {
    return $this->cacheContextThemeService;
  }

  /**
   * Sets the Drupal Cache Context Theme service.
   *
   * @param \Drupal\Core\Cache\Context\ThemeCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextThemeService(ThemeCacheContext $service) : void {
    $this->cacheContextThemeService = $service;
  }

}
