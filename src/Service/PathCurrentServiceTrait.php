<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Path\CurrentPathStack;

/**
 * Injection utility for the Drupal Path Current service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::PATH_CURRENT
 */
trait PathCurrentServiceTrait {

  /**
   * The Drupal Path Current service.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  private CurrentPathStack $pathCurrentService;

  /**
   * Gets the Drupal Path Current service.
   *
   * @return \Drupal\Core\Path\CurrentPathStack
   *   The Drupal Path Current service.
   */
  public function pathCurrentService() : CurrentPathStack {
    return $this->pathCurrentService;
  }

  /**
   * Sets the Drupal Path Current service.
   *
   * @param \Drupal\Core\Path\CurrentPathStack $service
   *   The service to be set.
   */
  public function setPathCurrentService(CurrentPathStack $service) : void {
    $this->pathCurrentService = $service;
  }

}
