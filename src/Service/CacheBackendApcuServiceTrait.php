<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\ApcuBackendFactory;

/**
 * Injection utility for the Drupal Cache Backend APCu service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_BACKEND_APCU
 */
trait CacheBackendApcuServiceTrait {

  /**
   * The Drupal Cache Backend APCu service.
   *
   * @var \Drupal\Core\Cache\ApcuBackendFactory
   */
  private ApcuBackendFactory $cacheBackendApcuService;

  /**
   * Gets the Drupal Cache Backend APCu service.
   *
   * @return \Drupal\Core\Cache\ApcuBackendFactory
   *   The Drupal Cache Backend APCu service.
   */
  public function cacheBackendApcuService() : ApcuBackendFactory {
    return $this->cacheBackendApcuService;
  }

  /**
   * Sets the Drupal Cache Backend APCu service.
   *
   * @param \Drupal\Core\Cache\ApcuBackendFactory $service
   *   The service to be set.
   */
  public function setCacheBackendApcuService(ApcuBackendFactory $service) : void {
    $this->cacheBackendApcuService = $service;
  }

}
