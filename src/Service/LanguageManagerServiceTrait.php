<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Language\LanguageManager;

/**
 * Injection utility for the Drupal Language Manager service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::LANGUAGE_MANAGER
 */
trait LanguageManagerServiceTrait {

  /**
   * The Drupal Language Manager service.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  private LanguageManager $languageManagerService;

  /**
   * Gets the Drupal Language Manager service.
   *
   * @return \Drupal\Core\Language\LanguageManager
   *   The Drupal Language Manager service.
   */
  public function languageManagerService() : LanguageManager {
    return $this->languageManagerService;
  }

  /**
   * Sets the Drupal Language Manager service.
   *
   * @param \Drupal\Core\Language\LanguageManager $service
   *   The service to be set.
   */
  public function setLanguageManagerService(LanguageManager $service) : void {
    $this->languageManagerService = $service;
  }

}
