<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\IpCacheContext;

/**
 * Injection utility for the Drupal Cache Context IP service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_IP
 */
trait CacheContextIpServiceTrait {

  /**
   * The Drupal Cache Context IP service.
   *
   * @var \Drupal\Core\Cache\Context\IpCacheContext
   */
  private IpCacheContext $cacheContextIpService;

  /**
   * Gets the Drupal Cache Context IP service.
   *
   * @return \Drupal\Core\Cache\Context\IpCacheContext
   *   The Drupal Cache Context IP service.
   */
  public function cacheContextIpService() : IpCacheContext {
    return $this->cacheContextIpService;
  }

  /**
   * Sets the Drupal Cache Context IP service.
   *
   * @param \Drupal\Core\Cache\Context\IpCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextIpService(IpCacheContext $service) : void {
    $this->cacheContextIpService = $service;
  }

}
