<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\PageCache\DefaultRequestPolicy;

/**
 * Injection utility for the Drupal Page Cache Request Policy service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::PAGE_CACHE_REQUEST_POLICY
 */
trait PageCacheRequestPolicyServiceTrait {

  /**
   * The Drupal Page Cache Request Policy service.
   *
   * @var \Drupal\Core\PageCache\DefaultRequestPolicy
   */
  private DefaultRequestPolicy $pageCacheRequestPolicyService;

  /**
   * Gets the Drupal Page Cache Request Policy service.
   *
   * @return \Drupal\Core\PageCache\DefaultRequestPolicy
   *   The Drupal Page Cache Request Policy service.
   */
  public function pageCacheRequestPolicyService() : DefaultRequestPolicy {
    return $this->pageCacheRequestPolicyService;
  }

  /**
   * Sets the Drupal Page Cache Request Policy service.
   *
   * @param \Drupal\Core\PageCache\DefaultRequestPolicy $service
   *   The service to be set.
   */
  public function setPageCacheRequestPolicyService(DefaultRequestPolicy $service) : void {
    $this->pageCacheRequestPolicyService = $service;
  }

}
