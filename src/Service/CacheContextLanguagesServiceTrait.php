<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\LanguagesCacheContext;

/**
 * Injection utility for the Drupal Cache Context Languages service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_LANGUAGES
 */
trait CacheContextLanguagesServiceTrait {

  /**
   * The Drupal Cache Context Languages service.
   *
   * @var \Drupal\Core\Cache\Context\LanguagesCacheContext
   */
  private LanguagesCacheContext $cacheContextLanguagesService;

  /**
   * Gets the Drupal Cache Context Languages service.
   *
   * @return \Drupal\Core\Cache\Context\LanguagesCacheContext
   *   The Drupal Cache Context Languages service.
   */
  public function cacheContextLanguagesService() : LanguagesCacheContext {
    return $this->cacheContextLanguagesService;
  }

  /**
   * Sets the Drupal Cache Context Languages service.
   *
   * @param \Drupal\Core\Cache\Context\LanguagesCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextLanguagesService(LanguagesCacheContext $service) : void {
    $this->cacheContextLanguagesService = $service;
  }

}
