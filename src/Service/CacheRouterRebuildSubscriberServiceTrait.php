<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\EventSubscriber\CacheRouterRebuildSubscriber;

/**
 * Injection utility for the Drupal Cache Router Rebuild Subscriber service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_ROUTER_REBUILD_SUBSCRIBER
 */
trait CacheRouterRebuildSubscriberServiceTrait {

  /**
   * The Drupal Cache Router Rebuild Subscriber service.
   *
   * @var \Drupal\Core\EventSubscriber\CacheRouterRebuildSubscriber
   */
  private CacheRouterRebuildSubscriber $cacheRouterRebuildSubscriberService;

  /**
   * Gets the Drupal Cache Router Rebuild Subscriber service.
   *
   * @return \Drupal\Core\EventSubscriber\CacheRouterRebuildSubscriber
   *   The Drupal Cache Router Rebuild Subscriber service.
   */
  public function cacheRouterRebuildSubscriberService() : CacheRouterRebuildSubscriber {
    return $this->cacheRouterRebuildSubscriberService;
  }

  /**
   * Sets the Drupal Cache Router Rebuild Subscriber service.
   *
   * @param \Drupal\Core\EventSubscriber\CacheRouterRebuildSubscriber $service
   *   The service to be set.
   */
  public function setCacheRouterRebuildSubscriberService(CacheRouterRebuildSubscriber $service) : void {
    $this->cacheRouterRebuildSubscriberService = $service;
  }

}
