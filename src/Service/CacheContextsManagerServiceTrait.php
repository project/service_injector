<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\CacheContextsManager;

/**
 * Injection utility for the Drupal Cache Contexts Manager service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXTS_MANAGER
 */
trait CacheContextsManagerServiceTrait {

  /**
   * The Drupal Cache Contexts Manager service.
   *
   * @var \Drupal\Core\Cache\Context\CacheContextsManager
   */
  private CacheContextsManager $cacheContextsManagerService;

  /**
   * Gets the Drupal Cache Contexts Manager service.
   *
   * @return \Drupal\Core\Cache\Context\CacheContextsManager
   *   The Drupal Cache Contexts Manager service.
   */
  public function cacheContextsManagerService() : CacheContextsManager {
    return $this->cacheContextsManagerService;
  }

  /**
   * Sets the Drupal Cache Contexts Manager service.
   *
   * @param \Drupal\Core\Cache\Context\CacheContextsManager $service
   *   The service to be set.
   */
  public function setCacheContextsManagerService(CacheContextsManager $service) : void {
    $this->cacheContextsManagerService = $service;
  }

}
