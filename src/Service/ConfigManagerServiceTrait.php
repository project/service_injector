<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Config\ConfigManager;

/**
 * Injection utility for the Drupal Config Manager service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CONFIG_MANAGER
 */
trait ConfigManagerServiceTrait {

  /**
   * The Drupal Config Manager service.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  private ConfigManager $configManagerService;

  /**
   * Gets the Drupal Config Manager service.
   *
   * @return \Drupal\Core\Config\ConfigManager
   *   The Drupal Config Manager service.
   */
  public function configManagerService() : ConfigManager {
    return $this->configManagerService;
  }

  /**
   * Sets the Drupal Config Manager service.
   *
   * @param \Drupal\Core\Config\ConfigManager $service
   *   The service to be set.
   */
  public function setConfigManagerService(ConfigManager $service) : void {
    $this->configManagerService = $service;
  }

}
