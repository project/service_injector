<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\UrlCacheContext;

/**
 * Injection utility for the Drupal Cache Context URL service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_URL
 */
trait CacheContextUrlServiceTrait {

  /**
   * The Drupal Cache Context URL service.
   *
   * @var \Drupal\Core\Cache\Context\UrlCacheContext
   */
  private UrlCacheContext $cacheContextUrlService;

  /**
   * Gets the Drupal Cache Context URL service.
   *
   * @return \Drupal\Core\Cache\Context\UrlCacheContext
   *   The Drupal Cache Context URL service.
   */
  public function cacheContextUrlService() : UrlCacheContext {
    return $this->cacheContextUrlService;
  }

  /**
   * Sets the Drupal Cache Context URL service.
   *
   * @param \Drupal\Core\Cache\Context\UrlCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextUrlService(UrlCacheContext $service) : void {
    $this->cacheContextUrlService = $service;
  }

}
