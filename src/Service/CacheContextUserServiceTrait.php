<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\UserCacheContext;

/**
 * Injection utility for the Drupal Cache Context User service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_USER
 */
trait CacheContextUserServiceTrait {

  /**
   * The Drupal Cache Context User service.
   *
   * @var \Drupal\Core\Cache\Context\UserCacheContext
   */
  private UserCacheContext $cacheContextUserService;

  /**
   * Gets the Drupal Cache Context User service.
   *
   * @return \Drupal\Core\Cache\Context\UserCacheContext
   *   The Drupal Cache Context User service.
   */
  public function cacheContextUserService() : UserCacheContext {
    return $this->cacheContextUserService;
  }

  /**
   * Sets the Drupal Cache Context User service.
   *
   * @param \Drupal\Core\Cache\Context\UserCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextUserService(UserCacheContext $service) : void {
    $this->cacheContextUserService = $service;
  }

}
