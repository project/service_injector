<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Injection utility for the Drupal Cache Discovery service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_DISCOVERY
 */
trait CacheDiscoveryServiceTrait {

  /**
   * The Drupal Cache Discovery service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private CacheBackendInterface $cacheDiscoveryService;

  /**
   * Gets the Drupal Cache Discovery service.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The Drupal Cache Discovery service.
   */
  public function cacheDiscoveryService() : CacheBackendInterface {
    return $this->cacheDiscoveryService;
  }

  /**
   * Sets the Drupal Cache Discovery service.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $service
   *   The service to be set.
   */
  public function setCacheDiscoveryService(CacheBackendInterface $service) : void {
    $this->cacheDiscoveryService = $service;
  }

}
