<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\IsSuperUserCacheContext;

/**
 * Injection utility for the Drupal Cache Context User Is Super User service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_USER_IS_SUPER_USER
 */
trait CacheContextUserIsSuperUserServiceTrait {

  /**
   * The Drupal Cache Context User Is Super User service.
   *
   * @var \Drupal\Core\Cache\Context\IsSuperUserCacheContext
   */
  private IsSuperUserCacheContext $cacheContextUserIsSuperUserService;

  /**
   * Gets the Drupal Cache Context User Is Super User service.
   *
   * @return \Drupal\Core\Cache\Context\IsSuperUserCacheContext
   *   The Drupal Cache Context User Is Super User service.
   */
  public function cacheContextUserIsSuperUserService() : IsSuperUserCacheContext {
    return $this->cacheContextUserIsSuperUserService;
  }

  /**
   * Sets the Drupal Cache Context User Is Super User service.
   *
   * @param \Drupal\Core\Cache\Context\IsSuperUserCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextUserIsSuperUserService(IsSuperUserCacheContext $service) : void {
    $this->cacheContextUserIsSuperUserService = $service;
  }

}
