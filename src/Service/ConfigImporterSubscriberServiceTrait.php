<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Config\Importer\FinalMissingContentSubscriber;

/**
 * Injection utility for the Drupal Config Importer Subscriber service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CONFIG_IMPORTER_SUBSCRIBER
 */
trait ConfigImporterSubscriberServiceTrait {

  /**
   * The Drupal Config Importer Subscriber service.
   *
   * @var \Drupal\Core\Config\Importer\FinalMissingContentSubscriber
   */
  private FinalMissingContentSubscriber $configImporterSubscriberService;

  /**
   * Gets the Drupal Config Importer Subscriber service.
   *
   * @return \Drupal\Core\Config\Importer\FinalMissingContentSubscriber
   *   The Drupal Config Importer Subscriber service.
   */
  public function configImporterSubscriberService() : FinalMissingContentSubscriber {
    return $this->configImporterSubscriberService;
  }

  /**
   * Sets the Drupal Config Importer Subscriber service.
   *
   * @param \Drupal\Core\Config\Importer\FinalMissingContentSubscriber $service
   *   The service to be set.
   */
  public function setConfigImporterSubscriberService(FinalMissingContentSubscriber $service) : void {
    $this->configImporterSubscriberService = $service;
  }

}
