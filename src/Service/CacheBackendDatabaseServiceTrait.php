<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\DatabaseBackendFactory;

/**
 * Injection utility for the Drupal Cache Backend Database service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_BACKEND_DATABASE
 */
trait CacheBackendDatabaseServiceTrait {

  /**
   * The Drupal Cache Backend Database service.
   *
   * @var \Drupal\Core\Cache\DatabaseBackendFactory
   */
  private DatabaseBackendFactory $cacheBackendDatabaseService;

  /**
   * Gets the Drupal Cache Backend Database service.
   *
   * @return \Drupal\Core\Cache\DatabaseBackendFactory
   *   The Drupal Cache Backend Database service.
   */
  public function cacheBackendDatabaseService() : DatabaseBackendFactory {
    return $this->cacheBackendDatabaseService;
  }

  /**
   * Sets the Drupal Cache Backend Database service.
   *
   * @param \Drupal\Core\Cache\DatabaseBackendFactory $service
   *   The service to be set.
   */
  public function setCacheBackendDatabaseService(DatabaseBackendFactory $service) : void {
    $this->cacheBackendDatabaseService = $service;
  }

}
