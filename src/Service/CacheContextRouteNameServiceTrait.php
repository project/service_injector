<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\RouteCacheContext;

/**
 * Injection utility for the Drupal Cache Context Route Name service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_ROUTE_NAME
 */
trait CacheContextRouteNameServiceTrait {

  /**
   * The Drupal Cache Context Route Name service.
   *
   * @var \Drupal\Core\Cache\Context\RouteCacheContext
   */
  private RouteCacheContext $cacheContextRouteNameService;

  /**
   * Gets the Drupal Cache Context Route Name service.
   *
   * @return \Drupal\Core\Cache\Context\RouteCacheContext
   *   The Drupal Cache Context Route Name service.
   */
  public function cacheContextRouteNameService() : RouteCacheContext {
    return $this->cacheContextRouteNameService;
  }

  /**
   * Sets the Drupal Cache Context Route Name service.
   *
   * @param \Drupal\Core\Cache\Context\RouteCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextRouteNameService(RouteCacheContext $service) : void {
    $this->cacheContextRouteNameService = $service;
  }

}
