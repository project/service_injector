<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Injection utility for the Drupal Config Factory service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CONFIG_FACTORY
 */
trait ConfigFactoryServiceTrait {

  /**
   * The Drupal Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private ConfigFactoryInterface $configFactoryService;

  /**
   * Gets the Drupal Config Factory service.
   *
   * @return \Drupal\Core\Config\ConfigFactoryInterface
   *   The Drupal Config Factory service.
   */
  public function configFactoryService() : ConfigFactoryInterface {
    return $this->configFactoryService;
  }

  /**
   * Sets the Drupal Config Factory service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $service
   *   The service to be set.
   */
  public function setConfigFactoryService(ConfigFactoryInterface $service) : void {
    $this->configFactoryService = $service;
  }

}
