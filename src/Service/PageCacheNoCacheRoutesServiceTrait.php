<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\PageCache\ResponsePolicy\DenyNoCacheRoutes;

/**
 * Injection utility for the Drupal Page Cache No Cache Routes service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::PAGE_CACHE_NO_CACHE_ROUTES
 */
trait PageCacheNoCacheRoutesServiceTrait {

  /**
   * The Drupal Page Cache No Cache Routes service.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\DenyNoCacheRoutes
   */
  private DenyNoCacheRoutes $pageCacheNoCacheRoutesService;

  /**
   * Gets the Drupal Page Cache No Cache Routes service.
   *
   * @return \Drupal\Core\PageCache\ResponsePolicy\DenyNoCacheRoutes
   *   The Drupal Page Cache No Cache Routes service.
   */
  public function pageCacheNoCacheRoutesService() : DenyNoCacheRoutes {
    return $this->pageCacheNoCacheRoutesService;
  }

  /**
   * Sets the Drupal Page Cache No Cache Routes service.
   *
   * @param \Drupal\Core\PageCache\ResponsePolicy\DenyNoCacheRoutes $service
   *   The service to be set.
   */
  public function setPageCacheNoCacheRoutesService(DenyNoCacheRoutes $service) : void {
    $this->pageCacheNoCacheRoutesService = $service;
  }

}
