<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\UserRolesCacheContext;

/**
 * Injection utility for the Drupal Cache Context User Roles service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_USER_ROLES
 */
trait CacheContextUserRolesServiceTrait {

  /**
   * The Drupal Cache Context User Roles service.
   *
   * @var \Drupal\Core\Cache\Context\UserRolesCacheContext
   */
  private UserRolesCacheContext $cacheContextUserRolesService;

  /**
   * Gets the Drupal Cache Context User Roles service.
   *
   * @return \Drupal\Core\Cache\Context\UserRolesCacheContext
   *   The Drupal Cache Context User Roles service.
   */
  public function cacheContextUserRolesService() : UserRolesCacheContext {
    return $this->cacheContextUserRolesService;
  }

  /**
   * Sets the Drupal Cache Context User Roles service.
   *
   * @param \Drupal\Core\Cache\Context\UserRolesCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextUserRolesService(UserRolesCacheContext $service) : void {
    $this->cacheContextUserRolesService = $service;
  }

}
