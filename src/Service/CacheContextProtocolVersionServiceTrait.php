<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\ProtocolVersionCacheContext;

/**
 * Injection utility for the Drupal Cache Context Protocol Version service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_PROTOCOL_VERSION
 */
trait CacheContextProtocolVersionServiceTrait {

  /**
   * The Drupal Cache Context Protocol Version service.
   *
   * @var \Drupal\Core\Cache\Context\ProtocolVersionCacheContext
   */
  private ProtocolVersionCacheContext $cacheContextProtocolVersionService;

  /**
   * Gets the Drupal Cache Context Protocol Version service.
   *
   * @return \Drupal\Core\Cache\Context\ProtocolVersionCacheContext
   *   The Drupal Cache Context Protocol Version service.
   */
  public function cacheContextProtocolVersionService() : ProtocolVersionCacheContext {
    return $this->cacheContextProtocolVersionService;
  }

  /**
   * Sets the Drupal Cache Context Protocol Version service.
   *
   * @param \Drupal\Core\Cache\Context\ProtocolVersionCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextProtocolVersionService(ProtocolVersionCacheContext $service) : void {
    $this->cacheContextProtocolVersionService = $service;
  }

}
