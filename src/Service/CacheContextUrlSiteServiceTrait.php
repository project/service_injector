<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\SiteCacheContext;

/**
 * Injection utility for the Drupal Cache Context URL Site service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_URL_SITE
 */
trait CacheContextUrlSiteServiceTrait {

  /**
   * The Drupal Cache Context URL Site service.
   *
   * @var \Drupal\Core\Cache\Context\SiteCacheContext
   */
  private SiteCacheContext $cacheContextUrlSiteService;

  /**
   * Gets the Drupal Cache Context URL Site service.
   *
   * @return \Drupal\Core\Cache\Context\SiteCacheContext
   *   The Drupal Cache Context URL Site service.
   */
  public function cacheContextUrlSiteService() : SiteCacheContext {
    return $this->cacheContextUrlSiteService;
  }

  /**
   * Sets the Drupal Cache Context URL Site service.
   *
   * @param \Drupal\Core\Cache\Context\SiteCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextUrlSiteService(SiteCacheContext $service) : void {
    $this->cacheContextUrlSiteService = $service;
  }

}
