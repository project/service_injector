<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Injection utility for the Drupal Cache Bootstrap service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_BOOTSTRAP
 */
trait CacheBootstrapServiceTrait {

  /**
   * The Drupal Cache Bootstrap service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private CacheBackendInterface $cacheBootstrapService;

  /**
   * Gets the Drupal Cache Bootstrap service.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The Drupal Cache Bootstrap service.
   */
  public function cacheBootstrapService() : CacheBackendInterface {
    return $this->cacheBootstrapService;
  }

  /**
   * Sets the Drupal Cache Bootstrap service.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $service
   *   The service to be set.
   */
  public function setCacheBootstrapService(CacheBackendInterface $service) : void {
    $this->cacheBootstrapService = $service;
  }

}
