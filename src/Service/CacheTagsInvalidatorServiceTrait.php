<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\CacheTagsInvalidator;

/**
 * Injection utility for the Drupal Cache Tags Invalidator service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_TAGS_INVALIDATOR
 */
trait CacheTagsInvalidatorServiceTrait {

  /**
   * The Drupal Cache Tags Invalidator service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidator
   */
  private CacheTagsInvalidator $cacheTagsInvalidatorService;

  /**
   * Gets the Drupal Cache Tags Invalidator service.
   *
   * @return \Drupal\Core\Cache\CacheTagsInvalidator
   *   The Drupal Cache Tags Invalidator service.
   */
  public function cacheTagsInvalidatorService() : CacheTagsInvalidator {
    return $this->cacheTagsInvalidatorService;
  }

  /**
   * Sets the Drupal Cache Tags Invalidator service.
   *
   * @param \Drupal\Core\Cache\CacheTagsInvalidator $service
   *   The service to be set.
   */
  public function setCacheTagsInvalidatorService(CacheTagsInvalidator $service) : void {
    $this->cacheTagsInvalidatorService = $service;
  }

}
