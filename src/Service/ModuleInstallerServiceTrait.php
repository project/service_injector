<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Extension\ModuleInstaller;

/**
 * Injection utility for the Drupal Module Installer service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::MODULE_INSTALLER
 */
trait ModuleInstallerServiceTrait {

  /**
   * The Drupal Module Installer service.
   *
   * @var \Drupal\Core\Extension\ModuleInstaller
   */
  private ModuleInstaller $moduleInstallerService;

  /**
   * Gets the Drupal Module Installer service.
   *
   * @return \Drupal\Core\Extension\ModuleInstaller
   *   The Drupal Module Installer service.
   */
  public function moduleInstallerService() : ModuleInstaller {
    return $this->moduleInstallerService;
  }

  /**
   * Sets the Drupal Module Installer service.
   *
   * @param \Drupal\Core\Extension\ModuleInstaller $service
   *   The service to be set.
   */
  public function setModuleInstallerService(ModuleInstaller $service) : void {
    $this->moduleInstallerService = $service;
  }

}
