<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\SessionCacheContext;

/**
 * Injection utility for the Drupal Cache Context Session service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_SESSION
 */
trait CacheContextSessionServiceTrait {

  /**
   * The Drupal Cache Context Session service.
   *
   * @var \Drupal\Core\Cache\Context\SessionCacheContext
   */
  private SessionCacheContext $cacheContextSessionService;

  /**
   * Gets the Drupal Cache Context Session service.
   *
   * @return \Drupal\Core\Cache\Context\SessionCacheContext
   *   The Drupal Cache Context Session service.
   */
  public function cacheContextSessionService() : SessionCacheContext {
    return $this->cacheContextSessionService;
  }

  /**
   * Sets the Drupal Cache Context Session service.
   *
   * @param \Drupal\Core\Cache\Context\SessionCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextSessionService(SessionCacheContext $service) : void {
    $this->cacheContextSessionService = $service;
  }

}
