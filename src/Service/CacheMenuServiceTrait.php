<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Injection utility for the Drupal Cache Menu service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_MENU
 */
trait CacheMenuServiceTrait {

  /**
   * The Drupal Cache Menu service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private CacheBackendInterface $cacheMenuService;

  /**
   * Gets the Drupal Cache Menu service.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The Drupal Cache Menu service.
   */
  public function cacheMenuService() : CacheBackendInterface {
    return $this->cacheMenuService;
  }

  /**
   * Sets the Drupal Cache Menu service.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $service
   *   The service to be set.
   */
  public function setCacheMenuService(CacheBackendInterface $service) : void {
    $this->cacheMenuService = $service;
  }

}
