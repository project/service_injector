<?php

namespace Drupal\service_injector\Service;

use Drupal\Component\Serialization\Json;

/**
 * Injection utility for the Drupal JSON Serialization service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::SERIALIZATION_JSON
 */
trait SerializationJsonServiceTrait {

  /**
   * The Drupal JSON Serialization service.
   *
   * @var \Drupal\Component\Serialization\Json
   */
  private Json $serializationJsonService;

  /**
   * Gets the Drupal JSON Serialization service.
   *
   * @return \Drupal\Component\Serialization\Json
   *   The Drupal JSON Serialization service.
   */
  public function serializationJsonService() : Json {
    return $this->serializationJsonService;
  }

  /**
   * Sets the Drupal JSON Serialization service.
   *
   * @param \Drupal\Component\Serialization\Json $service
   *   The service to be set.
   */
  public function setSerializationJsonService(Json $service) : void {
    $this->serializationJsonService = $service;
  }

}
