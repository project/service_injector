<?php

namespace Drupal\service_injector\Service;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Injection utility for the Symfony Request Stack service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::REQUEST_STACK
 */
trait RequestStackServiceTrait {

  /**
   * The Symfony Request Stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private RequestStack $requestStackService;

  /**
   * Gets the Symfony Request Stack service.
   *
   * @return \Symfony\Component\HttpFoundation\RequestStack
   *   The Symfony Request Stack service.
   */
  public function requestStackService() : RequestStack {
    return $this->requestStackService;
  }

  /**
   * Sets the Symfony Request Stack service.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $service
   *   The service to be set.
   */
  public function setRequestStackService(RequestStack $service) : void {
    $this->requestStackService = $service;
  }

}
