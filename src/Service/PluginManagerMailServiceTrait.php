<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Mail\MailManager;

/**
 * Injection utility for the Drupal Plugin Manager Mail service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::PLUGIN_MANAGER_MAIL
 */
trait PluginManagerMailServiceTrait {

  /**
   * The Drupal Plugin Manager Mail service.
   *
   * @var \Drupal\Core\Mail\MailManager
   */
  private MailManager $pluginManagerMailService;

  /**
   * Gets the Drupal Plugin Manager Mail service.
   *
   * @return \Drupal\Core\Mail\MailManager
   *   The Drupal Plugin Manager Mail service.
   */
  public function pluginManagerMailService() : MailManager {
    return $this->pluginManagerMailService;
  }

  /**
   * Sets the Drupal Plugin Manager Mail service.
   *
   * @param \Drupal\Core\Mail\MailManager $service
   *   The service to be set.
   */
  public function setPluginManagerMailService(MailManager $service) : void {
    $this->pluginManagerMailService = $service;
  }

}
