<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Config\ConfigInstaller;

/**
 * Injection utility for the Drupal Config Installer service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CONFIG_INSTALLER
 */
trait ConfigInstallerServiceTrait {

  /**
   * The Drupal Config Installer service.
   *
   * @var \Drupal\Core\Config\ConfigInstaller
   */
  private ConfigInstaller $configInstallerService;

  /**
   * Gets the Drupal Config Installer service.
   *
   * @return \Drupal\Core\Config\ConfigInstaller
   *   The Drupal Config Installer service.
   */
  public function configInstallerService() : ConfigInstaller {
    return $this->configInstallerService;
  }

  /**
   * Sets the Drupal Config Installer service.
   *
   * @param \Drupal\Core\Config\ConfigInstaller $service
   *   The service to be set.
   */
  public function setConfigInstallerService(ConfigInstaller $service) : void {
    $this->configInstallerService = $service;
  }

}
