<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Injection utility for the Drupal Cache Entity service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_ENTITY
 */
trait CacheEntityServiceTrait {

  /**
   * The Drupal Cache Entity service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private CacheBackendInterface $cacheEntityService;

  /**
   * Gets the Drupal Cache Entity service.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The Drupal Cache Entity service.
   */
  public function cacheEntityService() : CacheBackendInterface {
    return $this->cacheEntityService;
  }

  /**
   * Sets the Drupal Cache Entity service.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $service
   *   The service to be set.
   */
  public function setCacheEntityService(CacheBackendInterface $service) : void {
    $this->cacheEntityService = $service;
  }

}
