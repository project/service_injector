<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\PageCache\ResponsePolicy\NoServerError;

/**
 * Injection utility for the Drupal Page Cache No Server Error service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::PAGE_CACHE_NO_SERVER_ERROR
 */
trait PageCacheNoServerErrorServiceTrait {

  /**
   * The Drupal Page Cache No Server Error service.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\NoServerError
   */
  private NoServerError $pageCacheNoServerErrorService;

  /**
   * Gets the Drupal Page Cache No Server Error service.
   *
   * @return \Drupal\Core\PageCache\ResponsePolicy\NoServerError
   *   The Drupal Page Cache No Server Error service.
   */
  public function pageCacheNoServerErrorService() : NoServerError {
    return $this->pageCacheNoServerErrorService;
  }

  /**
   * Sets the Drupal Page Cache No Server Error service.
   *
   * @param \Drupal\Core\PageCache\ResponsePolicy\NoServerError $service
   *   The service to be set.
   */
  public function setPageCacheNoServerErrorService(NoServerError $service) : void {
    $this->pageCacheNoServerErrorService = $service;
  }

}
