<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\HeadersCacheContext;

/**
 * Injection utility for the Drupal Cache Context Headers service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_HEADERS
 */
trait CacheContextHeadersServiceTrait {

  /**
   * The Drupal Cache Context Headers service.
   *
   * @var \Drupal\Core\Cache\Context\HeadersCacheContext
   */
  private HeadersCacheContext $cacheContextHeadersService;

  /**
   * Gets the Drupal Cache Context Headers service.
   *
   * @return \Drupal\Core\Cache\Context\HeadersCacheContext
   *   The Drupal Cache Context Headers service.
   */
  public function cacheContextHeadersService() : HeadersCacheContext {
    return $this->cacheContextHeadersService;
  }

  /**
   * Sets the Drupal Cache Context Headers service.
   *
   * @param \Drupal\Core\Cache\Context\HeadersCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextHeadersService(HeadersCacheContext $service) : void {
    $this->cacheContextHeadersService = $service;
  }

}
