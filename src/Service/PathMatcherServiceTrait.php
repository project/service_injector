<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Path\PathMatcher;

/**
 * Injection utility for the Drupal Path Matcher service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::PATH_MATCHER
 */
trait PathMatcherServiceTrait {

  /**
   * The Drupal Path Matcher service.
   *
   * @var \Drupal\Core\Path\PathMatcher
   */
  private PathMatcher $pathMatherService;

  /**
   * Gets the Drupal Patch Matcher service.
   *
   * @return \Drupal\Core\Path\PathMatcher
   *   The Drupal Patch Matcher service.
   */
  public function pathMatherService() : PathMatcher {
    return $this->pathMatherService;
  }

  /**
   * Sets the Drupal Patch Matcher service.
   *
   * @param \Drupal\Core\Path\PathMatcher $service
   *   The service to be set.
   */
  public function setPathMatcherService(PathMatcher $service) : void {
    $this->pathMatherService = $service;
  }

}
