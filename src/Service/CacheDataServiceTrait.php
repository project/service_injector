<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Injection utility for the Drupal Cache Data service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_DATA
 */
trait CacheDataServiceTrait {

  /**
   * The Drupal Cache Data service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private CacheBackendInterface $cacheDataService;

  /**
   * Gets the Drupal Cache Data service.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The Drupal Cache Data service.
   */
  public function cacheDataService() : CacheBackendInterface {
    return $this->cacheDataService;
  }

  /**
   * Sets the Drupal Cache Data service.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $service
   *   The service to be set.
   */
  public function setCacheDataService(CacheBackendInterface $service) : void {
    $this->cacheDataService = $service;
  }

}
