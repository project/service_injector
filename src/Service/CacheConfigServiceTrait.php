<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Injection utility for the Drupal Cache Config service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONFIG
 */
trait CacheConfigServiceTrait {

  /**
   * The Drupal Cache Config service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private CacheBackendInterface $cacheConfigService;

  /**
   * Gets the Drupal Cache Config service.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The Drupal Cache Config service.
   */
  public function cacheConfigService() : CacheBackendInterface {
    return $this->cacheConfigService;
  }

  /**
   * Sets the Drupal Cache Config service.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $service
   *   The service to be set.
   */
  public function setCacheConfigService(CacheBackendInterface $service) : void {
    $this->cacheConfigService = $service;
  }

}
