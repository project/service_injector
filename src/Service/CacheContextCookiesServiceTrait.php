<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\CookiesCacheContext;

/**
 * Injection utility for the Drupal Cache Context Cookies service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_COOKIES
 */
trait CacheContextCookiesServiceTrait {

  /**
   * The Drupal Cache Context Cookies service.
   *
   * @var \Drupal\Core\Cache\Context\CookiesCacheContext
   */
  private CookiesCacheContext $cacheContextCookiesService;

  /**
   * Gets the Drupal Cache Context Cookies service.
   *
   * @return \Drupal\Core\Cache\Context\CookiesCacheContext
   *   The Drupal Cache Context Cookies service.
   */
  public function cacheContextCookiesService() : CookiesCacheContext {
    return $this->cacheContextCookiesService;
  }

  /**
   * Sets the Drupal Cache Context Cookies service.
   *
   * @param \Drupal\Core\Cache\Context\CookiesCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextCookiesService(CookiesCacheContext $service) : void {
    $this->cacheContextCookiesService = $service;
  }

}
