<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\RouteCacheContext;

/**
 * Injection utility for the Drupal Cache Context Route service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_ROUTE
 */
trait CacheContextRouteServiceTrait {

  /**
   * The Drupal Cache Context Route service.
   *
   * @var \Drupal\Core\Cache\Context\RouteCacheContext
   */
  private RouteCacheContext $cacheContextRouteService;

  /**
   * Gets the Drupal Cache Context Route service.
   *
   * @return \Drupal\Core\Cache\Context\RouteCacheContext
   *   The Drupal Cache Context Route service.
   */
  public function cacheContextRouteService() : RouteCacheContext {
    return $this->cacheContextRouteService;
  }

  /**
   * Sets the Drupal Cache Context Route service.
   *
   * @param \Drupal\Core\Cache\Context\RouteCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextRouteService(RouteCacheContext $service) : void {
    $this->cacheContextRouteService = $service;
  }

}
