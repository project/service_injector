<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\Context\IsFrontPathCacheContext;

/**
 * Injection utility for the Drupal Cache Context URL Path Is Front service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_CONTEXT_URL_PATH_IS_FRONT
 */
trait CacheContextUrlPathIsFrontServiceTrait {

  /**
   * The Drupal Cache Context URL Path Is Front service.
   *
   * @var \Drupal\Core\Cache\Context\IsFrontPathCacheContext
   */
  private IsFrontPathCacheContext $cacheContextUrlPathIsFrontService;

  /**
   * Gets the Drupal Cache Context URL Path Is Front service.
   *
   * @return \Drupal\Core\Cache\Context\IsFrontPathCacheContext
   *   The Drupal Cache Context URL Path Is Front service.
   */
  public function cacheContextUrlPathIsFrontService() : IsFrontPathCacheContext {
    return $this->cacheContextUrlPathIsFrontService;
  }

  /**
   * Sets the Drupal Cache Context URL Path Is Front service.
   *
   * @param \Drupal\Core\Cache\Context\IsFrontPathCacheContext $service
   *   The service to be set.
   */
  public function setCacheContextUrlPathIsFrontService(IsFrontPathCacheContext $service) : void {
    $this->cacheContextUrlPathIsFrontService = $service;
  }

}
