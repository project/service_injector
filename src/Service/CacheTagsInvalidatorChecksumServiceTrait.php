<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\DatabaseCacheTagsChecksum;

/**
 * Injection utility for the Drupal Cache Tags Invalidator Checksum service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_TAGS_INVALIDATOR_CHECKSUM
 */
trait CacheTagsInvalidatorChecksumServiceTrait {

  /**
   * The Drupal Cache Tags Invalidator Checksum service.
   *
   * @var \Drupal\Core\Cache\DatabaseCacheTagsChecksum
   */
  private DatabaseCacheTagsChecksum $cacheTagsInvalidatorChecksumService;

  /**
   * Gets the Drupal Cache Tags Invalidator Checksum service.
   *
   * @return \Drupal\Core\Cache\DatabaseCacheTagsChecksum
   *   The Drupal Cache Tags Invalidator Checksum service.
   */
  public function cacheTagsInvalidatorChecksumService() : DatabaseCacheTagsChecksum {
    return $this->cacheTagsInvalidatorChecksumService;
  }

  /**
   * Sets the Drupal Cache Tags Invalidator Checksum service.
   *
   * @param \Drupal\Core\Cache\DatabaseCacheTagsChecksum $service
   *   The service to be set.
   */
  public function setCacheTagsInvalidatorChecksumService(DatabaseCacheTagsChecksum $service) : void {
    $this->cacheTagsInvalidatorChecksumService = $service;
  }

}
