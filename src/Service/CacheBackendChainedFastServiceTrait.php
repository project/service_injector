<?php

namespace Drupal\service_injector\Service;

use Drupal\Core\Cache\ChainedFastBackendFactory;

/**
 * Injection utility for the Drupal Cache Backend Chained Fast service.
 *
 * @see \Drupal\service_injector\Constant\CoreServices::CACHE_BACKEND_CHAINEDFAST
 */
trait CacheBackendChainedFastServiceTrait {

  /**
   * The Drupal Cache Backend Chained Fast service.
   *
   * @var \Drupal\Core\Cache\ChainedFastBackendFactory
   */
  private ChainedFastBackendFactory $cacheBackendChainedFastService;

  /**
   * Gets the Drupal Cache Backend Chained Fast service.
   *
   * @return \Drupal\Core\Cache\ChainedFastBackendFactory
   *   The Drupal Cache Backend Chained Fast service.
   */
  public function cacheBackendChainedFastService() : ChainedFastBackendFactory {
    return $this->cacheBackendChainedFastService;
  }

  /**
   * Sets the Drupal Cache Backend Chained Fast service.
   *
   * @param \Drupal\Core\Cache\ChainedFastBackendFactory $service
   *   The service to be set.
   */
  public function setCacheBackendChainedFastService(ChainedFastBackendFactory $service) : void {
    $this->cacheBackendChainedFastService = $service;
  }

}
